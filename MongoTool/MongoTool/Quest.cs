﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace MongoTool
{
    public class Quest
    {
        private int id { get; set; }
        private string name { get; set; }
        private string tooltip { get; set; }
        private ObservableCollection<Item> rewards { get; set; }
        private string coords { get; set; }
        private string npc { get; set; }
        private string additionalInfo { get; set; }
        private int levelReq { get; set; }
        private int dungeonId { get; set; }

        public Quest()
        {

        }

        public Quest(int id, string name, string tooltip, ObservableCollection<Item> rewards, int dungeonId)
        {
            Id = id;
            Name = name;
            Tooltip = tooltip;
            Rewards = rewards;
            DungeonId = dungeonId;
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Tooltip
        {
            get
            {
                return tooltip;
            }
            set
            {
                tooltip = value;
                OnPropertyChanged("Tooltip");
            }
        }
        public ObservableCollection<Item> Rewards
        {
            get
            {
                return rewards;
            }
            set
            {
                rewards = value;
                OnPropertyChanged("Rewards");
            }
        }
        public string Coords
        {
            get { return coords; }
            set
            {
                coords = value;
                OnPropertyChanged("Coords");
            }
        }
        public string Npc
        {
            get { return npc; }
            set
            {
                npc = value;
                OnPropertyChanged("Npc");
            }
        }
        public string AdditionalInfo
        {
            get { return additionalInfo; }
            set
            {
                additionalInfo = value;
                OnPropertyChanged("AdditionalInfo");
            }
        }
        public int LevelReq
        {
            get { return levelReq; }
            set
            {
                levelReq = value;
                OnPropertyChanged("LevelReq");
            }
        }
        public int DungeonId
        {
            get
            {
                return dungeonId;
            }
            set
            {
                dungeonId = value;
            }
        }

        public static Quest GetQuests(int id, int dungeonId)
        {
            var client = new RestClient("https://classic.wowhead.com/tooltip/quest/" + id + "&json&power");
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            var quest = JsonConvert.DeserializeObject<Quest>(response.Content);
            quest.Id = id;
            quest.DungeonId = dungeonId;

            return quest;
        }

        //public static ObservableCollection<Quest> GetData()
        //{
        //    var questList = new ObservableCollection<Quest>
        //    {
        //        // RFC
        //        GetQuests(5722, 1),
        //        GetQuests(5723, 1),
        //        GetQuests(5761, 1),
        //        GetQuests(5725, 1),
        //        GetQuests(5728, 1),

        //        //WC
        //        GetQuests(962, 2),
        //        GetQuests(914, 2),
        //        GetQuests(1491, 2),
        //        GetQuests(959, 2),
        //        GetQuests(1486, 2),
        //        GetQuests(1487, 2),
        //        GetQuests(6981, 2),

        //        //SFK
        //        GetQuests(1013, 3),
        //        GetQuests(1098, 3),
        //        GetQuests(1014, 3),

        //        //BFD
        //        GetQuests(6563, 4),
        //        GetQuests(6561, 4),
        //        GetQuests(6921, 4),
        //        GetQuests(6565, 4),
        //        GetQuests(1198, 4),

        //        //Gnome
        //        GetQuests(2841, 5),
        //        GetQuests(2842, 5),
        //        GetQuests(2951, 5),
        //        GetQuests(2945, 5),
        //        GetQuests(2904, 5),

        //        //RFK
        //        GetQuests(1102, 6),
        //        GetQuests(1109, 6),
        //        GetQuests(6522, 6),
        //        GetQuests(1221, 6),
        //        GetQuests(1144, 6),

        //    };
        //    return questList;
        //}

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

       
    }
}
