﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MongoTool
{
    public class DungeonService
    {
        private static IMongoCollection<Dungeon> dungeonsCollection;
        public DungeonService(IMongoCollection<Dungeon> dCollection)
        {
            dungeonsCollection = dCollection;
        }
        public List<Dungeon> GetAllDungeons()
        {
            var allItems = dungeonsCollection
                .Find(new BsonDocument())
                .ToList();

            return allItems;
        }

        public List<Dungeon> SearchDungeonById(int id)
        {
            //var results = ItemsCollection.Find(_ => _.Id == id).Single();
            return dungeonsCollection.Find(x => x.Id == id).ToList();
        }

        public void InsertDungeon(Dungeon dungeon)
        {
            dungeonsCollection.InsertOne(dungeon);
        }
        public async Task UpdateDungeonAsync(Dungeon dungeon)
        {
            await dungeonsCollection.ReplaceOneAsync(tdi => tdi.Id == dungeon.Id, dungeon);
        }

        public async static Task<bool> DeleteDungeon(Dungeon dungeon)
        {
            var result = await dungeonsCollection.DeleteOneAsync(tdi => tdi.Id == dungeon.Id);

            return result.IsAcknowledged && result.DeletedCount == 1;
        }
    }
}
