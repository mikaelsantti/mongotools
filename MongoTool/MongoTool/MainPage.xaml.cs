using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MongoTool
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        static IMongoCollection<Item> itemsCollection;
        static IMongoCollection<Quest> questCollection;
        static IMongoCollection<Dungeon> dungeonCollection;
        readonly static string dbName = "xxxxx";
        readonly static string ItemCollection = "Items";
        readonly static string QuestCollection = "Quests";
        readonly static string DungeonCollection = "Dungeons";
        static MongoClient client;

        DungeonService dungeonService;
        QuestService questService;
        ItemService itemService;
        
        public MainPage()
        {
            InitializeComponent();
            dungeonService = new DungeonService(DungeonsCollection);
            questService = new QuestService(QuestsCollection);
            itemService = new ItemService(ItemsCollection);
            
            PopulatePickers();
            
        }
        public void PopulatePickers()
        {
            var dungeons = dungeonService.GetAllDungeons().OrderBy(x=>x.Name);
            dungeonPicker.ItemsSource = dungeons.ToList();

            var quests = questService.GetAllQuests().OrderBy(x=>x.Name);
            questPicker.ItemsSource = quests.ToList();

            var items = itemService.GetAllItems().OrderBy(x => x.Name);
            ItemPicker.ItemsSource = items.ToList();
        }

        private void QuestPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var quest = questPicker.SelectedItem as Quest;
            if (quest != null)
            {
                updateQuestName.Text = quest.Name;
                updateQuestCoords.Text = quest.Coords;
                updateQuestNpc.Text = quest.Npc;
                updateQuestLvl.Text = quest.LevelReq.ToString();
                updateQuestInfo.Text = quest.AdditionalInfo;
            }
        }
        private async void BtnInsertItem_Clicked(object sender, EventArgs e)
        {
            await InsertNewItemAsync();

            ItemPicker.ItemsSource = null;
            var items = itemService.GetAllItems().OrderBy(x => x.Name);
            ItemPicker.ItemsSource = items.ToList();
        }

        private async Task InsertNewItemAsync()
        {
            if (Convert.ToInt32(insertItemId.Text) < 1)
            {
                await DisplayAlert("Error", "Id was not valid", "Ok");
                return;
            }
            else if (questPicker.SelectedItem == null)
            {
                await DisplayAlert("Error", "Select quest", "Ok");
                return;
            }
            else
            {
                var quest = questPicker.SelectedItem as Quest;
                var item = Item.GetItem(Convert.ToInt32(insertItemId.Text), quest.Id);
                await itemService.InsertItemAsync(item);
            }
            
        }
        private async void BtnUpdateQuest_Clicked(object sender, EventArgs e)
        {
            var quest = questPicker.SelectedItem as Quest;
            if (updateQuestName.Text.Length > 0)
            {
                quest.Name = updateQuestName.Text;
            }
            if (updateQuestNpc.Text.Length > 0)
            {
                quest.Npc = updateQuestNpc.Text;
            }
            if (updateQuestCoords.Text.Length > 0)
            {
                quest.Coords = updateQuestCoords.Text;
            }
            if (updateQuestInfo.Text.Length > 0)
            {
                quest.AdditionalInfo = updateQuestInfo.Text;
            }
            if (Convert.ToInt32(updateQuestLvl.Text) > 8)
            {
                quest.LevelReq = Convert.ToInt32(updateQuestLvl.Text);
            }

            var success = await questService.UpdateQuest(quest);
            if (success > 0)
            {
                await DisplayAlert("Updated", "Updated fields.", "Ok");
            }
            Debug.WriteLine(success);

        }

        public static IMongoCollection<Item> ItemsCollection
        {
            get
            {
                string url = "mongodb://username:passwd@ds235053.mlab.com:35053/database";
                if (client == null || itemsCollection == null)
                {
                    MongoClientSettings settings = MongoClientSettings.FromUrl(
                        new MongoUrl(url));
                    settings.SslSettings = new SslSettings
                    {
                        EnabledSslProtocols = SslProtocols.Tls12
                    };

                    client = new MongoClient(settings);
                    var db = client.GetDatabase(dbName);

                    var collectionSettings = new MongoCollectionSettings
                    {
                        ReadPreference = ReadPreference.Nearest
                    };
                    itemsCollection = db.GetCollection<Item>(ItemCollection, collectionSettings);
                }
                return itemsCollection;
            }
        }
        public static IMongoCollection<Quest> QuestsCollection
        {
            get
            {
                string url = "mongodb://username:passwd@ds235053.mlab.com:35053/database";
                if (client == null || questCollection == null)
                {
                    MongoClientSettings settings = MongoClientSettings.FromUrl(
                        new MongoUrl(url));
                    settings.SslSettings = new SslSettings
                    {
                        EnabledSslProtocols = SslProtocols.Tls12
                    };

                    client = new MongoClient(settings);
                    var db = client.GetDatabase(dbName);

                    var collectionSettings = new MongoCollectionSettings
                    {
                        ReadPreference = ReadPreference.Nearest
                    };
                    questCollection = db.GetCollection<Quest>(QuestCollection, collectionSettings);
                }
                return questCollection;
            }
        }
        public static IMongoCollection<Dungeon> DungeonsCollection
        {
            get
            {
                string url = "mongodb://username:passwd@ds235053.mlab.com:35053/database";
                if (client == null || dungeonCollection == null)
                {
                    MongoClientSettings settings = MongoClientSettings.FromUrl(
                        new MongoUrl(url));
                    settings.SslSettings = new SslSettings
                    {
                        EnabledSslProtocols = SslProtocols.Tls12
                    };

                    client = new MongoClient(settings);
                    var db = client.GetDatabase(dbName);

                    var collectionSettings = new MongoCollectionSettings
                    {
                        ReadPreference = ReadPreference.Nearest
                    };
                    dungeonCollection = db.GetCollection<Dungeon>(DungeonCollection, collectionSettings);
                }
                return dungeonCollection;
            }
        }

        
    }
}
