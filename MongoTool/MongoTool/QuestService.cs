﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace MongoTool
{
    public class QuestService
    {
        private static IMongoCollection<Quest> questsCollection;
        public QuestService(IMongoCollection<Quest> qCollection)
        {
            questsCollection = qCollection;
        }
        public async Task<List<Quest>> GetAllQuestsAsync()
        {
            var allItems = await questsCollection
                .Find(new BsonDocument())
                .ToListAsync();

            return allItems;
        }
        public List<Quest> GetAllQuests()
        {
            var allItems = questsCollection
                .Find(new BsonDocument())
                .ToList();

            return allItems;
        }

        public List<Quest> SearchQuestById(int id)
        {
            //var results = ItemsCollection.Find(_ => _.Id == id).Single();            
            return questsCollection.Find(x => x.DungeonId == id).ToList();
        }

        public void InsertQuest(Quest quest)
        {
            questsCollection.InsertOne(quest);
        }
        public async Task<long> UpdateQuest(Quest quest)
        {
            var success = await questsCollection.ReplaceOneAsync(tdi => tdi.Id == quest.Id, quest);
            return success.ModifiedCount;
        }

        public async Task<bool> DeleteQuest(Quest quest)
        {
            var result = await questsCollection.DeleteOneAsync(tdi => tdi.Id == quest.Id);

            return result.IsAcknowledged && result.DeletedCount == 1;
        }
    }
}
