﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MongoTool
{
    public class ItemService
    {
        private static IMongoCollection<Item> itemsCollection;
        public ItemService(IMongoCollection<Item> iCollection)
        {
            itemsCollection = iCollection;
        }
        public async Task<List<Item>> GetAllItemsAsync()
        {
            var allItems = await itemsCollection
                .Find(new BsonDocument())
                .ToListAsync();

            return allItems;
        }
        public List<Item> GetAllItems()
        {
            var allItems = itemsCollection
                .Find(new BsonDocument())
                .ToList();

            return allItems;
        }

        public List<Item> SearchItemById(int id)
        {
            //var results = ItemsCollection.Find(_ => _.Id == id).Single();
            return itemsCollection.Find(x => x.QuestId == id).ToList();
        }

        public async Task InsertItemAsync(Item item)
        {
            await itemsCollection.InsertOneAsync(item);
        }
        public async static Task UpdateItem(Item item)
        {
            await itemsCollection.ReplaceOneAsync(tdi => tdi.Id == item.Id, item);
        }

        public async static Task<bool> DeleteItem(Item item)
        {
            var result = await itemsCollection.DeleteOneAsync(tdi => tdi.Id == item.Id);

            return result.IsAcknowledged && result.DeletedCount == 1;
        }
    }
}
