using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MongoTool
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public string Icon { get; set; }
        public string Tooltip { get; set; }
        public int QuestId { get; set; }
        public string ItemColor { get; set; }

        public Item()
        {
        }

        public Item(string name, int quality, string icon, string tooltip)
        {
            Name = name;
            Quality = quality;
            Icon = icon;
            Tooltip = tooltip;
        }

        public static ObservableCollection<Item> GetRewardList(List<int> rewardIDs, int questId)
        {
            ObservableCollection<Item> QuestRewardList;
            QuestRewardList = new ObservableCollection<Item>();
            foreach (var i in rewardIDs)
            {
                var item = GetItem(i);
                item.QuestId = questId;
                QuestRewardList.Add(item);
            }
            return QuestRewardList;
        }

        public static Item GetItem(int id)
        {
            var client = new RestClient("https://classic.wowhead.com/tooltip/item/" + id + "&json&power");
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            var i = JsonConvert.DeserializeObject<Item>(response.Content);
            i.Id = id;
            return i;
        }
        public static Item GetItem(int id, int questId)
        {
            var client = new RestClient("https://classic.wowhead.com/tooltip/item/" + id + "&json&power");
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            var i = JsonConvert.DeserializeObject<Item>(response.Content);
            i.Id = id;
            i.QuestId = questId;
            i.ItemColor = ItemRarityChecker(i);
            return i;
        }

        public static string ItemRarityChecker(Item item)
        {
            switch (item.Quality)
            {
                case 2:
                    return "#00d510";
                case 3:
                    return "#0070dd";
                default:
                    return "#ffffff";
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
