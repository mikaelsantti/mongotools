﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using RestSharp;
using Newtonsoft.Json;


namespace MongoTool
{
    public class Dungeon : INotifyPropertyChanged
    {
        private int id { get; set; }
        private string name { get; set; }
        private List<int> levelRange { get; set; }
        private ObservableCollection<Quest> quests { get; set; }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public List<int> LevelRange
        {
            get
            {
                return levelRange;
            }
            set
            {
                levelRange = value;
                OnPropertyChanged("LevelRange");
            }
        }
        public ObservableCollection<Quest> Quests
        {
            get
            {
                return quests;
            }
            set
            {
                quests = value;
                OnPropertyChanged("Quests");
            }
        }

        //public static ObservableCollection<Dungeon> GetData(ObservableCollection<Quest> QuestList)
        //{
        //    var dungeonList = new ObservableCollection<Dungeon>
        //    {
        //        new Dungeon { Id = 1, Name = "Ragefire Chasm", LevelRange = new List<int>(), Quests = QuestList },
        //        new Dungeon { Id = 2, Name = "Wailing Caverns", LevelRange = new List<int>(), Quests = QuestList },
        //        new Dungeon { Id = 3, Name = "Shadowfang Keep", LevelRange = new List<int>(), Quests = QuestList },
        //        new Dungeon { Id = 4, Name = "Blackfathom Deeps", LevelRange = new List<int>(), Quests = QuestList },
        //        new Dungeon { Id = 5, Name = "Gnomeregan", LevelRange = new List<int>(), Quests = QuestList },
        //        new Dungeon { Id = 6, Name = "Razorfen Kraul", LevelRange = new List<int>(), Quests = QuestList }
        //    };
        //    return dungeonList;
        //}

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public List<int> LevelRangeList(int level)
        {
            var range = new List<int>();
            range.Add(level);
            return range;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
